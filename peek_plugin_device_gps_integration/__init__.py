__version__ = "0.0.0"

from typing import Type

from peek_plugin_base.server.PluginLogicEntryHookABC import \
    PluginLogicEntryHookABC


def peekLogicEntryHook() -> Type[PluginLogicEntryHookABC]:
    from ._private.server.LogicEntryHook import LogicEntryHook

    return LogicEntryHook
