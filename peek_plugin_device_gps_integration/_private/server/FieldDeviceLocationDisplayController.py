from collections import defaultdict

from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks

from peek_core_device.server.DeviceApiABC import DeviceApiABC
from peek_core_user.server.UserApiABC import UserApiABC
from peek_plugin_device_gps_integration._private.server.PeekDeviceDispCreator import (
    PeekDeviceDispCreator,
)
from peek_plugin_diagram.server.DiagramImportApiABC import DiagramImportApiABC
from vortex.Payload import Payload


class FieldDeviceLocationDisplayController:
    def __init__(
        self,
        diagramImportApi: DiagramImportApiABC,
        deviceApi: DeviceApiABC,
        userApi: UserApiABC,
    ):
        self._diagramImportApi = diagramImportApi
        self._deviceApi = deviceApi
        self._userApi = userApi
        self._importedLookupTuples = defaultdict(list)
        self._peekDeviceDispCreator = PeekDeviceDispCreator(
            diagramImportApi=self._diagramImportApi
        )

    @inlineCallbacks
    def start(self):
        yield self._peekDeviceDispCreator.load()
        yield self.markFieldDeviceOnGisDiagram()
        self.setupHooks()

    def shutdown(self):
        self._peekDeviceDispCreator = None

    @inlineCallbacks
    def markFieldDeviceOnGisDiagram(self):
        # load all gps locations
        allFieldLoggedInUsers = yield self._userApi.infoApi.peekLoggedInDeviceTokens(
            isFieldDevice=True
        )
        allGpsLocations = yield self._deviceApi.deviceCurrentGpsLocations(
            allFieldLoggedInUsers
        )
        deviceMarks = yield self._peekDeviceDispCreator.generate(
            allGpsLocations)

        # encode icons
        fieldDeviceEncodedPayload = yield Payload(
            tuples=deviceMarks
        ).toEncodedPayloadDefer()

        # import to GIS diagram
        yield self._diagramImportApi.importDisps(
            "gisDiagram",
            "gisDiagram",
            PeekDeviceDispCreator.IMPORT_GROUP_HASH,
            fieldDeviceEncodedPayload,  # encoded shapes
        )

    def setupHooks(self):
        # on GPS location changes
        self._deviceApi.deviceCurrentGpsLocation().subscribe(
            lambda _: reactor.callLater(0, self.markFieldDeviceOnGisDiagram)
        )

        # on user login
        self._userApi.fieldHookApi.addPostLoginHook(
            lambda _: reactor.callLater(0, self.markFieldDeviceOnGisDiagram)
        )

        # on user logout
        self._userApi.fieldHookApi.addPostLogoutHook(
            lambda _: reactor.callLater(0, self.markFieldDeviceOnGisDiagram)
        )
