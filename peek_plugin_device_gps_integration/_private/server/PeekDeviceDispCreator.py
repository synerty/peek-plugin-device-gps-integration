import logging
from typing import List

from peek_core_device.tuples.DeviceGpsLocationTuple import \
    DeviceGpsLocationTuple
from peek_plugin_device_gps_integration._private.PluginNames import gisDiagram
from twisted.internet.defer import inlineCallbacks
from vortex.DeferUtil import deferToThreadWrapWithLogger
from vortex.Tuple import Tuple

from nztm import NZTMUtil
from nztm import WGS84Coordinates
from nztm import WGS84toNZTM
from peek_plugin_diagram.server.DiagramImportApiABC import DiagramImportApiABC
from peek_plugin_diagram.tuples.lookups.ImportDispColorTuple import \
    ImportDispColorTuple
from peek_plugin_diagram.tuples.lookups.ImportDispLayerTuple import \
    ImportDispLayerTuple
from peek_plugin_diagram.tuples.lookups.ImportDispLevelTuple import \
    ImportDispLevelTuple
from peek_plugin_diagram.tuples.lookups.ImportDispLineStyleTuple import (
    ImportDispLineStyleTuple,
)
from peek_plugin_diagram.tuples.lookups.ImportDispTextStyleTuple import (
    ImportDispTextStyleTuple,
)
from peek_plugin_diagram.tuples.shapes.ImportDispEllipseTuple import (
    ImportDispEllipseTuple,
)

logger = logging.getLogger(__name__)


class PeekDeviceDispCreator:
    IMPORT_GROUP_HASH = "peek_devices"

    _DISP_LAYER = ImportDispLayerTuple(
        order=100,
        name="PEEK Devices",
        visible=True,
        selectable=True,
        importHash="peek_devices",
        modelSetKey=gisDiagram,
    )

    _DISP_LEVEL_1 = ImportDispLevelTuple(
        order=100,
        name="PEEK Devices 1",
        minZoom=2,
        maxZoom=1000,
        importHash="peek_devices_1",
        modelSetKey=gisDiagram,
    )

    _DISP_LEVEL_2 = ImportDispLevelTuple(
        order=100,
        name="PEEK Devices 2",
        minZoom=1,
        maxZoom=2,
        importHash="peek_devices_2",
        modelSetKey=gisDiagram,
    )

    _DISP_LEVEL_3 = ImportDispLevelTuple(
        order=100,
        name="PEEK Devices 3",
        minZoom=0.4,
        maxZoom=1,
        importHash="peek_devices_3",
        modelSetKey=gisDiagram,
    )

    _DISP_LEVEL_4 = ImportDispLevelTuple(
        order=100,
        name="PEEK Devices 4",
        minZoom=0.1,
        maxZoom=0.4,
        importHash="peek_devices_4",
        modelSetKey=gisDiagram,
    )

    _DISP_LEVEL_5 = ImportDispLevelTuple(
        order=100,
        name="PEEK Devices 5",
        minZoom=0,
        maxZoom=0.1,
        importHash="peek_devices_5",
        modelSetKey=gisDiagram,
    )

    _DISP_TEXT_STYLE = ImportDispTextStyleTuple(
        name="PEEK Devices Text",
        scalable=True,
        scaleFactor=1,
        fontName="Courier",
        fontSize=14,
        importHash="peek_devices",
        modelSetKey=gisDiagram,
    )

    _DISP_COLOR = ImportDispColorTuple(
        name="PEEK Devices Color",
        color="#ff8000",
        modelSetKey=gisDiagram,
        importHash="peek_devices",
    )

    _DISP_LINE_STYLE = ImportDispLineStyleTuple(
        name="PEEK Devices Line " "Style",
        backgroundFillDashSpace=False,
        capStyle=ImportDispLineStyleTuple.CAP_BUTT,
        joinStyle=ImportDispLineStyleTuple.JOIN_BEVEL,
        winStyle=0,
        modelSetKey=gisDiagram,
        importHash="peek_devices",
    )

    def __init__(self, diagramImportApi: DiagramImportApiABC):
        self._diagramImportApi = diagramImportApi

    @inlineCallbacks
    def load(self) -> List[Tuple]:
        yield self._diagramImportApi.importLookups(
            gisDiagram,
            None,
            ImportDispLayerTuple.tupleType(),
            [self._DISP_LAYER],
            deleteOthers=False,
            updateExisting=True,
        )

        yield self._diagramImportApi.importLookups(
            gisDiagram,
            gisDiagram,
            ImportDispLevelTuple.tupleType(),
            [
                self._DISP_LEVEL_1,
                self._DISP_LEVEL_2,
                self._DISP_LEVEL_3,
                self._DISP_LEVEL_4,
                self._DISP_LEVEL_5,
            ],
            deleteOthers=False,
            updateExisting=True,
        )

        yield self._diagramImportApi.importLookups(
            gisDiagram,
            None,
            ImportDispTextStyleTuple.tupleType(),
            [self._DISP_TEXT_STYLE],
            deleteOthers=False,
            updateExisting=True,
        )

        yield self._diagramImportApi.importLookups(
            gisDiagram,
            None,
            ImportDispColorTuple.tupleType(),
            [self._DISP_COLOR],
            deleteOthers=False,
            updateExisting=True,
        )

        yield self._diagramImportApi.importLookups(
            gisDiagram,
            None,
            ImportDispLineStyleTuple.tupleType(),
            [self._DISP_LINE_STYLE],
            deleteOthers=False,
            updateExisting=True,
        )

    @deferToThreadWrapWithLogger(logger=logger)
    def generate(self, deviceLocations: List[DeviceGpsLocationTuple]):
        shapes = []
        for d in deviceLocations:
            shapes.extend(self._makeShapes(d))
        return shapes

    def _makeShapes(self, deviceLocation: DeviceGpsLocationTuple):
        levels = [
            self._DISP_LEVEL_1,
            self._DISP_LEVEL_2,
            self._DISP_LEVEL_3,
            self._DISP_LEVEL_4,
            self._DISP_LEVEL_5,
        ]
        shapes = []
        for idx, level in enumerate(levels, 1):
            shapes.append(
                self._makeTestShape(
                    deviceLocation, level.importHash, size=idx ** 3 * 20
                )
            )
        return shapes

    def _makeTestShape(self, deviceLocation, levelHash: str, size=20):
        # convert to nztm format
        wgs64Location = WGS84Coordinates(
            latitude=deviceLocation.latitude,
            longitude=deviceLocation.longitude,
            unit=NZTMUtil.DEG,
        )
        nztmLocation = WGS84toNZTM(wgs64Location, precision=2)
        e, n = nztmLocation.easting, nztmLocation.northing

        pin = ImportDispEllipseTuple(
            key=deviceLocation.deviceToken,
            selectable=True,
            overlay=True,
            levelHash=levelHash,
            layerHash=self._DISP_LAYER.importHash,
            importGroupHash=self.IMPORT_GROUP_HASH,
            modelSetKey=gisDiagram,
            coordSetKey=gisDiagram,
            lineWidth=0,
            lineStyleHash=self._DISP_LINE_STYLE.importHash,
            lineColorHash=self._DISP_COLOR.importHash,
            fillColorHash=self._DISP_COLOR.importHash,
            geom=[],
            liveDbDispLinks=[],
        )

        pin.geom = [[e, n]]
        pin.xRadius = size
        pin.yRadius = size / 5 * 4

        pin.startAngle = 288
        pin.endAngle = 252
        return pin
