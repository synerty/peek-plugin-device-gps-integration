import logging
from typing import List

from twisted.internet import defer
from twisted.internet.defer import inlineCallbacks

from peek_core_device.server.DeviceApiABC import DeviceApiABC
from peek_core_device.tuples.DeviceDetailTuple import DeviceDetailTuple
from peek_core_device.tuples.DeviceGpsLocationTuple import DeviceGpsLocationTuple
from peek_core_docdb._private.server.api.DocDbApi import DocDbApi
from peek_core_docdb.tuples.ImportDocumentTuple import ImportDocumentTuple
from peek_core_search._private.server.api.SearchApi import SearchApi
from peek_core_search.tuples.ImportSearchObjectTuple import ImportSearchObjectTuple
from peek_core_user._private.server.api.UserApi import UserApi
from peek_core_user.tuples.DeviceWithUserDetails import (
    DeviceWithUserDetailsTuple,
)
from peek_core_user.tuples.UserDetailTuple import UserDetailTuple
from peek_core_user.tuples.login.UserLoginResponseTuple import UserLoginResponseTuple
from peek_core_user.tuples.login.UserLogoutResponseTuple import UserLogoutResponseTuple
from twisted.internet import defer
from twisted.internet.defer import inlineCallbacks
from vortex.Payload import Payload
from vortex.VortexUtil import debounceCall

logger = logging.getLogger(__name__)


class DeviceSearchController:
    def __init__(
        self,
        deviceApi: DeviceApiABC,
        searchApi: SearchApi,
        docDbApi: DocDbApi,
        userApi: UserApi,
    ):
        self._deviceApi = deviceApi
        self._searchApi = searchApi
        self._docDbApi = docDbApi
        self._userApi = userApi

    def start(self):
        self.setupHooks()

    def shutdown(self):
        # on GPS updates
        self._deviceApi.deviceCurrentGpsLocation().dispose()

        # on user login
        self._userApi.fieldHookApi.removePostLoginHook(self.onUserLogin)

        # on user logout
        self._userApi.fieldHookApi.removePostLogoutHook(self.onUserLogout)

    @inlineCallbacks
    def _loadFieldLoggedInUsers(self) -> List[DeviceWithUserDetailsTuple]:
        # load all users who logged in on Field app
        users = yield self._userApi.infoApi.peekTokensWithUserDetails(
            isFieldDevice=True
        )
        return users

    @inlineCallbacks
    def _loadAllDevices(self):
        allDeviceTokens = yield self._deviceApi.deviceTokens()
        return allDeviceTokens

    @inlineCallbacks
    def _loadFieldLoggedInDevices(self) -> List[DeviceDetailTuple]:
        # load device details given device tokens
        deviceTokens = yield self._userApi.infoApi.peekLoggedInDeviceTokens(
            isFieldDevice=True
        )
        devices = yield self._deviceApi.deviceDetails(deviceTokens)
        return devices

    @inlineCallbacks
    def _updateCoreSearch(
        self, fieldDeviceWithUserDetailsTuples: List[DeviceWithUserDetailsTuple]
    ):
        searchObjects = []
        fieldUsersByToken = {
            o.deviceToken: o.userDetails for o in fieldDeviceWithUserDetailsTuples
        }
        allDeviceTokens = yield self._loadAllDevices()

        usersByToken = {}
        for deviceToken in allDeviceTokens:
            usersByToken[deviceToken] = fieldUsersByToken.get(
                deviceToken, UserDetailTuple()
            )

        for deviceToken, userDetails in usersByToken.items():
            searchObject = ImportSearchObjectTuple(
                key=deviceToken,
                objectType="peek_device",
                fullKeywords={},
                partialKeywords={
                    "email": userDetails.email or "",
                    "mobile": userDetails.mobile or "",
                    "userName": userDetails.userName or "",
                    "displayName": userDetails.userTitle or "",
                },
                routes=[],
            )
            searchObjects.append(searchObject)
        payload = yield Payload(tuples=searchObjects).toEncodedPayloadDefer()
        yield self._searchApi.importSearchObjects(payload)

    @debounceCall(60)
    @inlineCallbacks
    def _updateDocDb(self):
        devices = yield self._loadFieldLoggedInDevices()
        deviceTokens = [d.deviceToken for d in devices]
        deviceDetails = yield self._deviceApi.deviceDetails(deviceTokens)
        deviceCurrentGpsLocations = yield self._deviceApi.deviceCurrentGpsLocations(
            deviceTokens
        )
        deviceDetailDict = {}
        for d in deviceDetails:
            deviceDetailDict[d.deviceToken] = d

        devicedeviceCurrentGpsLocationDict = {}
        for d in deviceCurrentGpsLocations:
            devicedeviceCurrentGpsLocationDict[d.deviceToken] = d

        docs = []
        for deviceToken in deviceTokens:
            userDetail = yield self._userApi.infoApi.peekUserForDeviceToken(deviceToken)
            if not userDetail:
                # fallback to none if userDetail is None (e.g. not logged in
                # onto peek field)
                userDetail = UserDetailTuple()
            location = devicedeviceCurrentGpsLocationDict[deviceToken]
            deviceDetail = deviceDetailDict[deviceToken]
            doc = ImportDocumentTuple(
                key=deviceToken,
                modelSetKey="pofDiagram",
                documentTypeKey="peek_device",
                importGroupHash="peek_device",
                document={
                    "latitude": location.latitude,
                    "longitude": location.longitude,
                    "datetime": location.datetime,
                    "onlineStatus": deviceDetail.deviceStatusDisplayText,
                    "lastOnline": deviceDetail.lastOnlineDisplayText,
                    "deviceToken": deviceToken,
                    "user": userDetail.userName,
                    "userTitle": userDetail.userTitle,
                    "email": userDetail.email,
                    "mobile": userDetail.mobile,
                },
            )
            docs.append(doc)
        payload = yield Payload(tuples=docs).toEncodedPayloadDefer()
        yield self._docDbApi.createOrUpdateDocuments(payload)

    @inlineCallbacks
    def updateAllFieldLoggedInUsers(self):
        users = yield self._loadFieldLoggedInUsers()
        yield self._updateCoreSearch(users)

    @inlineCallbacks
    def updateAllLoggedInFieldDevices(self):
        yield self._updateDocDb()

    @inlineCallbacks
    def onGpsUpdate(self, _: DeviceGpsLocationTuple):
        yield self.updateAllLoggedInFieldDevices()

    @inlineCallbacks
    def onUserLogin(self, response: UserLoginResponseTuple):
        if not response.succeeded:
            return response

        yield self.updateAllFieldLoggedInUsers()

    @inlineCallbacks
    def onUserLogout(self, response: UserLogoutResponseTuple):
        if not response.succeeded:
            return response

        yield self.updateAllFieldLoggedInUsers()

    def setupHooks(self):
        # on GPS updates
        self._deviceApi.deviceCurrentGpsLocation().subscribe(self.onGpsUpdate)

        # on user login
        self._userApi.fieldHookApi.addPostLoginHook(self.onUserLogin)

        # on user logout
        self._userApi.fieldHookApi.addPostLogoutHook(self.onUserLogout)
