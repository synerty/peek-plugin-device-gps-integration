import logging

from peek_core_device._private.server.DeviceApi import DeviceApi
from peek_core_docdb._private.server.api.DocDbApi import DocDbApi
from peek_core_search._private.server.api.SearchApi import SearchApi
from peek_core_user._private.server.api.UserApi import UserApi
from peek_plugin_base.server.PluginLogicEntryHookABC import \
    PluginLogicEntryHookABC
from peek_plugin_device_gps_integration._private.server.DeviceSearchController import (
    DeviceSearchController,
)
from peek_plugin_device_gps_integration._private.server.FieldDeviceLocationDisplayController import (
    FieldDeviceLocationDisplayController,
)
from peek_plugin_diagram.server.DiagramApiABC import DiagramApiABC

logger = logging.getLogger(__name__)


class LogicEntryHook(
    PluginLogicEntryHookABC,
):
    def __init__(self, *args, **kwargs):
        """" Constructor """
        # Call the base classes constructor
        PluginLogicEntryHookABC.__init__(self, *args, **kwargs)

        #: Loaded Objects, This is a list of all objects created when we start
        self._loadedObjects = []

    def load(self) -> None:
        """Load

        This will be called when the plugin is loaded, just after the db is migrated.
        Place any custom initialiastion steps here.

        """
        logger.debug("Loaded")

    def start(self):
        """Start

        This will be called to start the plugin.
        Start, means what ever we choose to do here. This includes:

        -   Create Controllers

        -   Create payload, observable and tuple action handlers.

        """
        deviceApi: DeviceApi = self.platform.getOtherPluginApi(
            "peek_core_device")
        searchApi: SearchApi = self.platform.getOtherPluginApi(
            "peek_core_search")
        docDbApi: DocDbApi = self.platform.getOtherPluginApi("peek_core_docdb")
        userApi: UserApi = self.platform.getOtherPluginApi("peek_core_user")
        diagramApi: DiagramApiABC = self.platform.getOtherPluginApi(
            "peek_plugin_diagram"
        )
        deviceSearchController = DeviceSearchController(
            deviceApi=deviceApi,
            userApi=userApi,
            searchApi=searchApi,
            docDbApi=docDbApi,
        )
        self._loadedObjects.append(deviceSearchController)
        deviceSearchController.start()

        fieldDeviceLocationDisplayController = FieldDeviceLocationDisplayController(
            diagramImportApi=diagramApi.importApi(),
            deviceApi=deviceApi,
            userApi=userApi,
        )
        fieldDeviceLocationDisplayController.start()
        self._loadedObjects.append(fieldDeviceLocationDisplayController)
        logger.debug("Started")

    def stop(self):
        """Stop

        This method is called by the platform to tell the peek app to shutdown and stop
        everything it's doing
        """
        # Shutdown and dereference all objects we constructed when we started
        while self._loadedObjects:
            self._loadedObjects.pop().shutdown()

        logger.debug("Stopped")

    def unload(self):
        """Unload

        This method is called after stop is called, to unload any last resources
        before the PLUGIN is unlinked from the platform

        """
        logger.debug("Unloaded")
